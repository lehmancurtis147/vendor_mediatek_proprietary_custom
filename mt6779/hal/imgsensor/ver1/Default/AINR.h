const FEATURE_NVRAM_AINR_T AINR_%04d = {
    .chi_R = 500, .chi_GR = 500, .chi_GB = 500, .chi_B = 500,
    .std_R = 5000, .std_GR = 5000, .std_GB = 5000, .std_B =5000,
    .AINR_blend_R = 1000000, .AINR_blend_GR = 1000000, .AINR_blend_GB = 1000000, .AINR_blend_B = 1000000,
    .MIX_blend_Y = 64, .MIX_blend_UV = 0, .Lambda = 0,
    .rsv1 = 0, .rsv2 = 0, .rsv3 = 0, .rsv4 = 0,.rsv5 = 0, .rsv6 = 0, .rsv7 = 0,
};